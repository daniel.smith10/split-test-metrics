import pandas as pd
from pathlib import Path

from pb_ml.dao.sql import PBMLSqlClient


class ClickThroughRate:

    def __init__(self):
        self.data_dir = Path(__file__).parent / 'data'
        self.query_dir = Path(__file__).parent / 'queries'
        self.sql_client = PBMLSqlClient(engine_name='snowflake', credentials_file=None)

    def get_click_through_rate(self, df: pd.DataFrame):
        session_pbids = self.get_session_unique_pbids(df)
        non_session_pbids = self.get_other_session_viewed_pbids(session_id=df['session_id'].iloc[0],
                                                                user_id=df['user_id'].iloc[0])
        previously_viewed_count = len(set(session_pbids).intersection(set(non_session_pbids)))
        viewed_in_session_count = len(session_pbids)

        return previously_viewed_count / viewed_in_session_count

    @staticmethod
    def get_session_unique_pbids(dataframe: pd.DataFrame) -> list:
        """
        Gets the unique pbids from the dataframe

        :param dataframe: the dataframe
        :return: a list of unique pbids
        """
        not_null = dataframe['pb_id'][dataframe['pb_id'].notnull()]

        return not_null.unique().tolist()

    def get_other_session_viewed_pbids(self, session_id: int, user_id: int) -> list:
        """
        Gets the pbids of other sessions that viewed the same vcp
        """
        query = (self.query_dir / 'viewed_pbids_from_non_target_session.sql').read_text().format(session_id=session_id,
                                                                                                 user_id=user_id)
        return self.sql_client.execute_query(query)['pb_id'].tolist()


if __name__ == '__main__':
    ctr = ClickThroughRate()
    data_file_path = Path(__file__).parent / "data" / "session_14572421.csv"
    data = pd.read_csv(data_file_path)
    print(f"Session Click Through Rate: {ctr.get_click_through_rate(data) * 100:.2f}%")

