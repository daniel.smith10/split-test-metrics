import re
from pathlib import Path

import pandas as pd


class BounceRate:
    non_gs_event = re.compile(r'^(?!GS).*')
    entity_click = re.compile(r'^GS._ENTITY_CLICK')
    gsr_view = re.compile(r'GSR_VIEW')
    session_bounce_sequence = [gsr_view, entity_click, non_gs_event, re.compile(r'GS*')]
    last_action_bounce_sequence = [gsr_view, entity_click, non_gs_event]
    no_bounce_sequence = [gsr_view, entity_click, non_gs_event, non_gs_event]

    def get_session_bounce_rate(self, df: pd.DataFrame) -> float:
        """
        Calculates the bounce rate for a given session

        :param df: a dataframe containing the session data
        :return: a float representing the bounce rate
        """
        bounces = 0
        no_bounces = 0
        prev_frame = None
        for frame in self.iterate_over_frame_with_rolling_window(df):
            if prev_frame is not None:
                if self.sequence_is_bounce(prev_frame):
                    bounces += 1
                elif self.sequence_is_not_bounce(prev_frame):
                    no_bounces += 1
            prev_frame = frame

        if prev_frame is not None:
            if self.sequence_is_bounce(prev_frame, last_frame=True):
                bounces += 1
            elif self.sequence_is_not_bounce(prev_frame):
                no_bounces += 1

        return bounces / (bounces + no_bounces)

    @staticmethod
    def iterate_over_frame_with_rolling_window(df: pd.DataFrame, window: int = 4):
        """
        Iterates over a dataframe with a rolling window of size window
        """
        for index in range(0, df.shape[0] + 1 - window):
            yield df.iloc[index:index + window]

    def sequence_is_bounce(self, frame: pd.DataFrame, last_frame: bool = False) -> bool:
        """
        Checks if a sequence of events is a bounce

        :param frame: a dataframe containing a sample of events from a session
        :param last_frame: a boolean check to see if the frame is the last frame in the session
        :return: True if a bounce event otherwise False
        """
        if last_frame:
            is_bounce = self.get_last_sequence_bounce(frame)
        else:
            is_bounce = self.get_intermediate_sequence_bounce(frame)

        return is_bounce

    def get_last_sequence_bounce(self, frame: pd.DataFrame) -> bool:
        """
        Checks if a terminal sequence of events is a bounce

        :param frame: a dataframe containing a sample of events from a session
        :return: True if all zipped regexes and event codes match otherwise False
        """
        return all(regex.match(event_code) for regex, event_code in
                   zip(self.last_action_bounce_sequence, frame['event_code'][1:]))

    def get_intermediate_sequence_bounce(self, frame: pd.DataFrame) -> bool:
        """
        Checks if a non-terminal sequence of events is a bounce

        :param frame: a dataframe containing a sample of events from a session
        :return: True if all zipped regexes and event codes match otherwise False
        """
        return all(regex.match(event_code) for regex, event_code in
                   zip(self.session_bounce_sequence, frame['event_code']))

    def sequence_is_not_bounce(self, frame: pd.DataFrame) -> bool:
        """
        Checks if a sequence of events is a bounce

        :param frame: a dataframe containing a sample of events from a session
        :return: True if all zipped regexes and event codes match otherwise False
        """
        return all(regex.match(event_code) for regex, event_code in
                   zip(self.no_bounce_sequence, frame['event_code']))


if __name__ == '__main__':
    ex = BounceRate()
    data_file_path = Path(__file__).parent / "data" / "session_14572421.csv"
    data = pd.read_csv(data_file_path)
    print(f"Session Bounce Rate: {ex.get_session_bounce_rate(data) * 100:.2f}%")

