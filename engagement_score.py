from pathlib import Path
from pb_ml.dao.sql import PBMLSqlClient

from click_through_rate import ClickThroughRate
from dwell_time import DwellTime
from bounce_rate import BounceRate


class Engagement:
    weights = {'bounceRate': 0.333,
               'clickThroughRate': 0.333,
               'dwellTime': 0.333}

    def __init__(self):
        self.sql_client = PBMLSqlClient(engine_name='snowflake', credentials_file=None)
        self.query_dir = Path(__file__).parent / 'queries'
        self.data_dir = Path(__file__).parent / 'data'
        self.click_through_rate_fetcher = ClickThroughRate()
        self.bounce_rate_fetcher = BounceRate()
        self.dwell_time_fetcher = DwellTime()

    def get_engagement_score(self, session_id: int):
        session_events = self.get_session_events_by_id(session_id)

        dwell_time = self.dwell_time_fetcher.get_session_dwell_rate(session_events)
        bounce_rate = self.bounce_rate_fetcher.get_session_bounce_rate(session_events)
        click_through_rate = self.click_through_rate_fetcher.get_click_through_rate(session_events)

        engagement_score = (click_through_rate * self.weights['clickThroughRate']) + (
                    dwell_time * self.weights['dwellTime']) - (bounce_rate * self.weights['bounceRate'])

        return {
            'bounceRate': bounce_rate,
            'clickThroughRate': click_through_rate,
            'dwellTime': dwell_time,
            'engagementScore': engagement_score
        }

    def get_session_events_by_id(self, session_id: int):
        query = self.query_dir / 'session_events.sql'
        query = query.read_text()
        query = query.format(session_id=session_id)

        query_result = self.sql_client.execute_query(query)
        query_result.to_csv(self.data_dir / f'session_{session_id}.csv', index=False)

        return query_result


if __name__ == '__main__':
    ex = Engagement()
    result = ex.get_engagement_score(14572421)
    from pprint import pprint
    pprint(result)