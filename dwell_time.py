from pathlib import Path
from typing import Tuple

import pandas as pd


class DwellTime:

    def get_session_dwell_rate(self, df: pd.DataFrame) -> float:
        """
        Calculates the dwell rate for a given session

        :param df: a dataframe containing the session data
        :return: a float representing the dwell rate, which is a percentage of session time spent dwelling on content
        outside the General Search Results page
        """
        session_duration = self.get_session_duration(df)
        dwell_time_session = []
        index = 1
        while index < df.shape[0]:
            example_dwell_time, index = self.get_dwell_event_duration(df, index)
            if example_dwell_time <= 0:
                break
            dwell_time_session.append(example_dwell_time)

        return sum(dwell_time_session) / session_duration

    @staticmethod
    def get_session_duration(df: pd.DataFrame) -> int:
        """
        Calculates the duration of a session in seconds
        """
        return (pd.Timestamp(df.event_date_pst.max()) - pd.Timestamp(df.event_date_pst.min())).seconds

    def get_dwell_event_duration(self, dataframe: pd.DataFrame, start_index=1) -> Tuple[float, int]:
        """
        Calculates the duration of a dwell event in seconds.

        :param dataframe: A subsection of a session dataframe.
        :param start_index: the index to start the search for a dwell event
        :return: get back a tuple of dwell time for this event and the index of the last row in the dwell event
        """
        dwelling_on_vcp = False
        data_points = dataframe.shape[0]

        start_time = None
        finish_time = None
        last_index = None

        for index in range(start_index, data_points):
            previous_row = dataframe.loc[index - 1]
            current_row = dataframe.loc[index]
            previous_row_is_gs = self.is_gsr_event(previous_row)
            current_row_is_gs = self.is_gsr_event(current_row)

            if previous_row_is_gs and not current_row_is_gs and not dwelling_on_vcp:
                dwelling_on_vcp = True
                start_time = current_row['event_date_pst']

            if not previous_row_is_gs and current_row_is_gs and dwelling_on_vcp:
                dwelling_on_vcp = False
                finish_time = current_row['event_date_pst']
                last_index = index
                break

        if dwelling_on_vcp:
            finish_time = current_row['event_date_pst']
            last_index = index

        dwell_time = (pd.Timestamp(finish_time) - pd.Timestamp(start_time)).seconds

        return dwell_time, last_index

    @staticmethod
    def is_gsr_event(row):
        return row['event_code'].startswith('GS')


if __name__ == '__main__':
    ex = DwellTime()
    data_file_path = Path(__file__).parent / "data" / "session_14572421.csv"
    data = pd.read_csv(data_file_path)
    print(f"Session Dwell Time: {ex.get_session_dwell_rate(data) * 100:.2f}%")
