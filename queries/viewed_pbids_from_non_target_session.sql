SELECT DISTINCT ae.PB_ID
FROM "BUSINESS_INTELLIGENCE"."USAGE"."ALL_EVENTS" ae
WHERE ae.SESSION_ID != {session_id}
  AND ae.USER_ID = {user_id}
  AND ae.PB_ID IS NOT NULL