SELECT
  100.0 * (
    -- number of results clicked on in current session that were also clicked on in other sessions
    SELECT COUNT(DISTINCT ae.PB_ID)
    FROM "BUSINESS_INTELLIGENCE"."USAGE"."ALL_EVENTS" ae
    WHERE ae.SESSION_ID = {session_id1}
      AND ae.PB_ID IS NOT NULL
      AND ae.PB_ID IN (
        SELECT DISTINCT e2.PB_ID
        FROM "BUSINESS_INTELLIGENCE"."USAGE"."ALL_EVENTS" e2
        WHERE e2.SESSION_ID != {session_id2}
          AND e2.PB_ID IS NOT NULL
      )
  ) / (
    -- Number of results displayed in current session general search
    SELECT COUNT(DISTINCT e.PB_ID)
    FROM "BUSINESS_INTELLIGENCE"."USAGE"."ALL_EVENTS" e
    WHERE e.SESSION_ID = {session_id3}
      AND e.PB_ID IS NOT NULL
  ) AS "PREVIOUSLY_VIEWED_CLICK_THROUGH_RATE";